<?php 

return array(
	

	/*
	|----------------------------------------------------------
	| Encryption Keys
	|----------------------------------------------------------
	|  These keys are for the security of your app, the first should be string
	|  contains 32 chars and the second should be string contains at least 10
	|  chars, in first configuration the framework change automatically these
	|  keys
	*/

	'key1' => 'e7ab1496ad9a4bfa3fd7993015bd0f61',
	'key2' => '40aeab0cd0bd22fca929154f4757b1bb',

);