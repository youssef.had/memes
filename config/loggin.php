<?php

return [

	/*
	|----------------------------------------------------------
	| Allow Debug
	|----------------------------------------------------------
	|  Here to make the framework shows errors and
	|  exceptions, false to show friendly messages
	|  and true to debug
	|
	**/
	'debug' => true ,


	/*
	|----------------------------------------------------------
	| Error log
	|----------------------------------------------------------
	|  The path of log file where Vinala store errors
	|
	**/
	'log' => 'storage/log/vinala.log' ,


];